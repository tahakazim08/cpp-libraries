//Basic C++ functions simplified 
//Made by Taha Rawjani

#include "functions.h"
#include <iostream>
#include <string>

namespace lib{

    int add(int x,int y){
        return x+y;
        }
    int doubleNumber(int x){
        return x*2;
        }
    int subtract(int x,int y){
        return x-y;
        }
    int compare(int x,int y){
        if (x==y){return 0;} //equal
        else if (x<=y){return 1;}
        else if (x>=y){return -1;}
        else {return 1290390;}
    }
    std::string getString(std::string y){ //Function that returns a string

        std::cout<< y;

        std::string x{};
        std::getline(std::cin>>std::ws,x);
        return x;
    }
    int getInt(std::string y){
        std::cout<< y;
        
        int x{};
        std::cin>> x;
        return x;
    }
    double getDouble(std::string y){
        std::cout<< y;
        
        double x{};
        std::cin>> x;
        return x;
    }
}