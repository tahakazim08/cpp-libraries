#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include<string>

namespace lib{
int add(int x,int y);
int doubleNumber(int x);
int compare(int x,int y);
std::string getString(std::string y);
int getInt(std::string y);
double getDouble(std::string y);
}
#endif